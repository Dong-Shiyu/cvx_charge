
#train,val,test=load_data(".")
#model=load_pretrained_model()
import torch
from schnetpack.data.atoms import AtomsConverter
class ChargeNet():
    def __init__(self,model_pt=None):
        model_pt="/home/sydong/work/deepchem/cvxcharge/all_pos_model/all_pos_cpu"
        self.model=torch.load(model_pt)
        self.para_model=self.model[0]
    def calc(self,atoms,total_charge):
        """Input is ase and total_charge"""
        converter=AtomsConverter(device="cpu")
        inp=converter(atoms)
        inp["total_charge"]=torch.tensor([[total_charge]],dtype=torch.float32)
        with torch.no_grad():
            charge=self.model(inp).detach().numpy()
        return charge
    def __call__(self,atoms):
        return self.calc(atoms,0.0)
    def parameter(self,atoms):
        converter=AtomsConverter(device="cpu")
        inp=converter(atoms)
        with torch.no_grad():
            paras=self.para_model(inp)
            out={}
            out['atomic_neg']=paras['atomic_neg']
            out['atomic_hard']=paras['atomic_hard']
            out['atomic_width']=paras['atomic_width']
            return  out

if __name__=="__main__":
    import ase 
    calc=ChargeNet()
    water=ase.build.molecule("H2O")
    charge=calc.calc(water,0.0)
    paras=calc.parameter(water)
    print(charge)
    print(paras)
