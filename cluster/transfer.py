import os
for i in range(2,31):
    with open("w_{}.xyz".format(i)) as fhand:
        lines=fhand.readlines()
        content=[]
        for line in lines:
            coord=line.strip().split()
            if len(coord)==4:
                coord[0]=coord[0][0]
                content.append(coord)
    with open("water_{}.xyz".format(i),"w") as fhand:
        fhand.write("{} \n\n".format(i*3))
        for info in content:
            data="{}  {}  {}  {} \n".format(*info)
            fhand.writelines(data)
