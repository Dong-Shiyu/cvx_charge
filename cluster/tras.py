import os
with open("w_32.xyz") as fhand:
    lines=fhand.readlines()
    content=[]
    for line in lines:
        data=line.strip().split()
        content.append(data[:4])
        content.append(data[4:])
    with open("water_32.xyz","w") as fhand:
        fhand.writelines("96\n\n")
        for data in content:
            fhand.writelines("{} {} {} {} \n".format(*data))
