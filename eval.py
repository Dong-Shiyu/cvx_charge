from hogwild.get_model import load_pretrained_model
from hogwild.get_data import load_data
model=load_pretrained_model()

from schnetpack.data.atoms import AtomsConverter
from ase.build import molecule
from sklearn.linear_model import LinearRegression

import torch
import pdb
pdb.set_trace()
water=molecule("H2O")
converter=AtomsConverter(device="cuda")
inp=converter(water)
train,val,test=load_data('.')
max_mae=0
model.eval()
with torch.no_grad():
    for i,batch in enumerate(test):
        single_inp={k:v.to("cuda") for k,v in batch.items()}
        single_out=model(single_inp)
        mae=torch.abs(single_inp['monopoles']-single_out).mean() 
        print(mae)
        X=single_inp['monopoles'].cpu().detach().numpy().flatten().reshape(-1,1)
        y=single_out.cpu().detach().numpy().flatten()
        reg=LinearRegression().fit(X,y)
        print(reg.score(X,y))
        if mae > max_mae:
            max_mae=mae
    print(max_mae)
