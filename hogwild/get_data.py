import numpy as np
import os
import torch
from schnetpack.data import AtomsData 
from schnetpack.data import AtomsLoader
from schnetpack.data import create_subset
from schnetpack.atomistic  import Atomwise
__all__=["load_data","load_stat"]
def load_data(root_path=None,batch_size=32,shuffle=True,num_workers=8):
    """
    Total four type of data is get.
    atom data contains atom ,charge etc. 
    split data  file contains train test val split file
    corrupt data  file contains data not correct need to remove
    stata data statistical of data need during train. 

    Inputs root path a single path constains file  
    """
    # processing path

    print("root path is {}".format(os.path.abspath(root_path)))
    atom_pt=os.path.abspath(root_path+'/data/chargeall.db')
    split_pt=os.path.abspath(root_path+'/data/splitall.npz')
    corrupt_pt=os.path.abspath(root_path+'/dataprocess/corrupt.npy')

    #loading data from processed data
    try:
        data=AtomsData(atom_pt,load_only=['monopoles','molecular_dipole'])
        split_file=np.load(split_pt)
        corrupt=np.load(corrupt_pt)
    except:
        print("The root is where root/data/chargeall.db")
        # create split index and remove corrupt
    train_index=split_file['train_idx'].tolist()
    val_index=split_file['val_idx'].tolist()
    test_index=split_file['test_idx'].tolist()
    for i in corrupt :
        if i in train_index:
            train_index.remove(i)
        if i in val_index:
            val_index.remove(i)
        if i in test_index:
            test_index.remove(i)
    # create split 
    train_dataset=create_subset(data,train_index)
    val_dataset=create_subset(data,val_index)
    test_dataset=create_subset(data,test_index)

    # create dataloader
    train_loader=AtomsLoader(train_dataset,batch_size=batch_size,shuffle=shuffle,num_workers=num_workers)
    val_loader=AtomsLoader(val_dataset,batch_size=batch_size,num_workers=num_workers)
    test_loader=AtomsLoader(test_dataset,batch_size=batch_size,num_workers=num_workers)
    # print dataset and laoder  information    
    print('Data prepare finished')
    print('training batch number {}'.format(len(train_loader)))
    print('training point {}'.format(len(train_dataset)))
    print('validation batch number {}'.format(len(val_loader)))
    print('validation point {}'.format(len(val_dataset)))
    # statistic 
    return train_loader , val_loader  ,test_loader
if __name__== "__main__":
    load_stat()
    load_data("..")

