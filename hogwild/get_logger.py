
# log 



LOG_PATH='./log/cvxtest'
writer=SummaryWriter(LOG_PATH)
#dummy_input1=list(iter(val_loader))[0]
#writer.add_graph(model,(dummy_input1))
# one epoch train loop
LOG_BATCH_NUM=30
MODEL_PATH='./model/cvxgputest/final_model'
BEST_MODEL_PATH='./model/cvxgputest/best_model'
CHECK_PATH='./model/cvxgputest/check'
CHECK_EPOCH_NUM=1
BEST_VAL_LOSS=1e13
CHECK_NUM=5
if  not os.path.exists(CHECK_PATH):
    os.makedirs(CHECK_PATH)
def save_checkpoint(epoch,model,optimizer,loss):
    chkpths=[]
    if epoch %CHECK_EPOCH_NUM==0:
        state_dict={
            'epoch':epoch,
            'model_state_dict':model.state_dict(),
            'optimizer_state_dict':optimizer.state_dict(),
            'loss':loss}
        chkpth=os.path.abspath(os.path.join(CHECK_PATH,'checkpoint-'+str(epoch)+'.pth.tar'))
        chkpths.append(chkpth)
        torch.save(state_dict,chkpth)
    if len(chkpths)>CHECK_NUM:
        os.remove(chkpths[0]) 
def save_best(model,loss):
    global BEST_VAL_LOSS
    if loss<BEST_VAL_LOSS :
        BEST_VAL_LOSS=loss 
