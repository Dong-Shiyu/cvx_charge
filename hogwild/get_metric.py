import torch

__all__=["cvx_loss_fn","validation_mas"]

#loss function
def loss_fn(batch,result,rho=0.5):
    if batch['monopoles'].shape != result.charge.shape:
        print(batch['monopoles'].shape)
        print(result.charge.shape)
    diff_charge=batch['monopoles']-result.charge  # shape (n_batch * n_atoms*nprop )
    diff_dipole=batch['molecular_dipole']-result.dipoles # shape(n_batch*nprp) 
    rmse_charge=torch.mean(diff_charge**2)
    rmse_dipole=torch.mean(diff_dipole**2)
    loss=rho*rmse_charge +(1-rho) * rmse_dipole
    return loss
def cvx_loss_fn(batch,result):
    diff_charge=batch['monopoles']-result
    mse_charge=torch.mean(diff_charge**2)
    return mse_charge
#validation 
def validation_mae(batch,result):
    diff_charge=batch['monopoles']-result  
#    diff_dipole=batch['molecular_dipole']-result['dipoles'] # shape(n_batch*nprp) 
    l1loss=torch.sum(torch.abs(diff_charge)).detach().cpu().data.numpy()   
    n_atoms=torch.sum(batch['_atom_mask']).detach().cpu().data.numpy()
    mae=l1loss/n_atoms
    return mae
