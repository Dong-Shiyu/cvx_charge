import numpy as np
import torch
import os
import schnetpack as spk
from schnetpack.atomistic  import Atomwise

from mynet.model import AtomisticModel
from mynet.qplayer import  QPLayer

__all__=["load_model","load_pretrained_model"]

def load_para():
    """Load charge equilibration parameters"""
    element=[1,6,7,8,9,16,17]
    
    NEGATIVITY=np.zeros((32,1))
    HARDNESS=np.zeros((32,1))
    RADIAS=np.zeros((32,1))
    # data from J. Chem. Theory Comput. 2011, 7, 6, 1750–1764
    #Publication Date:May 2, 2011
    #https://doi.org/10.1021/ct200006e
    negativity=[6.37,7.37,8.03,9.23,16.31,6.98,10.19,8.11]
    hardness=[15.54	,10.74,10.93,12.85,38.37,7.86,31.89,31.94]
    # atom radial
    atom_r=[0.371,0.759,0.715,0.669,0.706,1.047,0.994]

    for i ,e in enumerate(element):
        NEGATIVITY[e]=negativity[i]
    for i ,e in enumerate(element):
        HARDNESS[e]=hardness[i]
    for i ,e in enumerate(element):
        RADIAS[e]=atom_r[i]
        
    print(NEGATIVITY.shape)
    print(HARDNESS.shape)
    print(RADIAS.shape)
    return NEGATIVITY , HARDNESS , RADIAS
NEGATIVITY , HARDNESS , RADIAS=load_para()
def load_model():
    """ load predefined model with fixed parameters"""
    schnet=spk.representation.SchNet(
                 n_atom_basis=128,n_filters=128,n_gaussians=50,n_interactions=6,
                 cutoff=10,cutoff_network=spk.nn.cutoff.CosineCutoff)
    negativity_model=Atomwise(
                  n_in=128,property="molecule_neg",contributions="atomic_neg",
             #     atomref=NEGATIVITY
                  )
    hardness_model=Atomwise(
                  n_in=128,property="molecule_hard",contributions="atomic_hard",
             #     atomref=NEGATIVITY
                  )
    width_model=Atomwise(
                  n_in=128,property="molecule_width",contributions="atomic_width",
             #     atomref=NEGATIVITY
                  )
    output=[negativity_model,hardness_model,width_model]
    parameter_model=AtomisticModel(representation=schnet,output_modules=output)
    opt_model=QPLayer()
    model=torch.nn.Sequential(parameter_model,opt_model)
    return model
def load_pretrained_model(model_path=None):
    if model_path==None:
        model_path=os.path.join(os.path.dirname(__file__),"final_cpu_model.pt")
    print("model at {}".format(os.path.abspath(model_path))) 
    pretrained_model=torch.load(model_path) 
    return pretrained_model
if __name__=="__main__":
   model=load_model()
   print(model)
