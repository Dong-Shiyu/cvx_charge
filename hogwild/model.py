import numpy as np
import os
import torch
import datetime
#from torch.utils.tensorboard import SummaryWritter
torch.manual_seed(1)
import pdb
import schnetpack as spk
import schnetpack.train as trn
from schnetpack.data import AtomsData 
from schnetpack.data import AtomsLoader
from schnetpack.data import create_subset
from schnetpack.atomistic  import Atomwise
#from mynet.output_modules import Atomwise
#from mynet.output_modules import DipoleMoment

#from mynet.output_modules import Atomwise
from mynet.model import AtomisticModel
#from schnetpack import AtomisticModel
from mynet.qplayer import  QPLayer
from tensorboardX import SummaryWriter
# use this to test model correct do not train.
#load data 
path0='./data/chargeall.db'
path1='./chargesmall.db'
data=AtomsData(path1,load_only=['monopoles','molecular_dipole'])
#train,val,test=spk.train_test_split(data=data,num_train=300,num_val=64,
#    split_file="./split.npz")
#split_file=np.load('./data/splitall.npz')
split_file=np.load('./splitsmall.npz')
train_index=split_file['train_idx'].tolist()
val_index=split_file['val_idx'].tolist()
#corrpt=np.load('corrupt.npy')
#for i in corrpt :
#    if i in train_index:
#        train_index.remove(i)
#    if i in val_index:
#        val_index.remove(i)
train_dataset=create_subset(data,train_index)
val_dataset=create_subset(data,val_index)
train_loader=AtomsLoader(train_dataset,batch_size=32,shuffle=True,num_workers=8)
val_loader=AtomsLoader(val_dataset,batch_size=32,num_workers=8)
    
print('Data prepare finished')
print('training batch number {}'.format(len(train_loader)))
print('training point {}'.format(len(train_dataset)))
print('validation batch number {}'.format(len(val_loader)))
print('validation point {}'.format(len(val_dataset)))
# statistic 
from dataprocess.stat import TRAIN_MEAN,TRAIN_STDDEV,NEGATIVITY,HARDNESS,RADIAS

print(TRAIN_MEAN.shape)
print(TRAIN_STDDEV.shape)
print(NEGATIVITY.shape)
print(HARDNESS.shape)
print(RADIAS.shape)
schnet=spk.representation.SchNet(
             n_atom_basis=128,n_filters=128,n_gaussians=50,n_interactions=6,
             cutoff=10,cutoff_network=spk.nn.cutoff.CosineCutoff)
#output=[DipoleMoment(n_in=128,charge_correction=True,
#             contributions='charge',property='dipoles',predict_magnitude=False,
#             elemental_mean=TRAIN_MEAN,
#             elemental_stddev=TRAIN_STDDEV)]
negativity_model=Atomwise(
              n_in=128,property="molecule_neg",contributions="atomic_neg",
              atomref=NEGATIVITY
              )
hardness_model=Atomwise(
              n_in=128,property="molecule_hard",contributions="atomic_hard",
              atomref=NEGATIVITY
              )
width_model=Atomwise(
              n_in=128,property="molecule_width",contributions="atomic_width",
              atomref=NEGATIVITY
              )
output=[negativity_model,hardness_model,width_model]
parameter_model=AtomisticModel(representation=schnet,output_modules=output)
opt_model=QPLayer()
model=torch.nn.Sequential(parameter_model,opt_model)

model.cuda()
#loss function
rho=0.5
def loss_fn(batch,result):
    if batch['monopoles'].shape != result.charge.shape:
        print(batch['monopoles'].shape)
        print(result.charge.shape)
    diff_charge=batch['monopoles']-result.charge  # shape (n_batch * n_atoms*nprop )
    diff_dipole=batch['molecular_dipole']-result.dipoles # shape(n_batch*nprp) 
    rmse_charge=torch.mean(diff_charge**2)
    rmse_dipole=torch.mean(diff_dipole**2)
    loss=rho*rmse_charge +(1-rho) * rmse_dipole
    return loss
def cvx_loss_fn(batch,result):
    diff_charge=batch['monopoles']-result
    mse_charge=torch.mean(diff_charge**2)
    return mse_charge
#validation 
def validation_mae(batch,result):
    diff_charge=batch['monopoles']-result  
#    diff_dipole=batch['molecular_dipole']-result['dipoles'] # shape(n_batch*nprp) 
    l1loss=torch.sum(torch.abs(diff_charge)).detach().cpu().data.numpy()   
    n_atoms=torch.sum(batch['_atom_mask']).detach().cpu().data.numpy()
    mae=l1loss/n_atoms
    return mae
#optimizer
optimizer=torch.optim.Adam(model.parameters(),lr=5e-4)
#learning rate scheduler 
scheduler=torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,mode='min',factor=0.5,
        patience=1,min_lr=1e-6)
# log 
LOG_PATH='./log/cvxtest'
writer=SummaryWriter(LOG_PATH)
#dummy_input1=list(iter(val_loader))[0]
#writer.add_graph(model,(dummy_input1))
# one epoch train loop
LOG_BATCH_NUM=30
MODEL_PATH='./model/cvxgputest/final_model'
BEST_MODEL_PATH='./model/cvxgputest/best_model'
CHECK_PATH='./model/cvxgputest/check'
CHECK_EPOCH_NUM=1
BEST_VAL_LOSS=1e13
CHECK_NUM=5
if  not os.path.exists(CHECK_PATH):
    os.makedirs(CHECK_PATH)
def save_checkpoint(epoch,model,optimizer,loss):
    chkpths=[]
    if epoch %CHECK_EPOCH_NUM==0:
        state_dict={
            'epoch':epoch,
            'model_state_dict':model.state_dict(),
            'optimizer_state_dict':optimizer.state_dict(),
            'loss':loss}
        chkpth=os.path.abspath(os.path.join(CHECK_PATH,'checkpoint-'+str(epoch)+'.pth.tar'))
        chkpths.append(chkpth)
        torch.save(state_dict,chkpth)
    if len(chkpths)>CHECK_NUM:
        os.remove(chkpths[0]) 
def save_best(model,loss):
    global BEST_VAL_LOSS
    if loss<BEST_VAL_LOSS :
        BEST_VAL_LOSS=loss 
        torch.save(model,BEST_MODEL_PATH)
#GPU Train 

           
def one_epoch_train(epoch_index): 
    print("Train epoch {} begin".format(epoch_index))
    training_loss=0 # train_loop
    model.train()
    log_begin=datetime.datetime.today()
    for batch_index,train_batch in enumerate(train_loader,start=1): 
        optimizer.zero_grad()
        train_input= {k: v.to('cuda') for k, v in train_batch.items()}
        train_out=model(train_input)
        batch_loss=cvx_loss_fn(train_input,train_out)
        batch_loss.backward()
        optimizer.step()
        training_loss+=batch_loss.item()
        if batch_index % LOG_BATCH_NUM==LOG_BATCH_NUM-1: 
            training_loss/=LOG_BATCH_NUM
            print("Train loss at {} {}".format(epoch_index*len(train_loader)+batch_index,training_loss),)
            writer.add_scalar('Train/loss', training_loss, epoch_index*len(train_loader)+batch_index)
            training_loss=0
    model.eval()
    validation_loss=0.0
    metric_loss=0.0
    # validataion loop 
    val_begin=datetime.datetime.today()
    for validation_batch in val_loader:
        val_input={k: v.to('cuda') for k, v in validation_batch.items()} 
        val_out=model(val_input)
        val_loss=cvx_loss_fn(val_input,val_out)
        m_loss=validation_mae(val_input,val_out)
        validation_loss+=val_loss.item()
        metric_loss+=m_loss.item()
    val_end=datetime.datetime.today()
    val_time=val_end-val_begin
    print('validation time {}'.format(val_time))
    validation_loss/=len(val_loader)
    metric_loss/=len(val_dataset)
    x_value=epoch_index*len(train_loader)+batch_index
    print("Validation loss {}".format(validation_loss))
    writer.add_scalar('Validataion/loss',validation_loss,x_value)
    writer.add_scalar('metric/mae',metric_loss,x_value)
    writer.add_scalar('learning rate',optimizer.param_groups[0]['lr'],x_value)
    print("mean absolute error {}".format(metric_loss))
    scheduler.step(validation_loss)
    stop=optimizer.param_groups[0]['lr']<=scheduler.min_lrs[0]
    # save and check
    save_checkpoint(epoch_index,model,optimizer,validation_loss)
    save_best(model,validation_loss)
    log_end=datetime.datetime.today()
    log_time=log_end-log_begin
    print("log frequency {}".format(log_time))
    log_begin=log_end
    print('epoch {}'.format(epoch_index))
    return stop
NUM_EPOCHS=30000000
print('model_prepare finished')
for epoch in range(NUM_EPOCHS):
    epoch_begin=datetime.datetime.today()
    stop_criteria=one_epoch_train(epoch)
    epoch_end=datetime.datetime.today()
    print("one epoch time {}".format(epoch_end-epoch_begin))
    if stop_criteria:
        torch.save(model.state_dict(),MODEL_PATH)
        print('early stop')
        break
writer.close()
