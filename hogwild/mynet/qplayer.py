#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 17:29:32 2022

@author: sydong
"""
import torch
import cvxpy as cp
import numpy as np
from cvxpylayers.torch import CvxpyLayer


def build_qp_layers(n):
    Q_sqrt = cp.Parameter((n, n))
    q = cp.Parameter((n))
    A = cp.Parameter((1, n))
    b = cp.Parameter((1))
    x = cp.Variable(n)
    obj = cp.Minimize(0.5 * cp.sum_squares(Q_sqrt @ x) + q.T @ x)
    cons = [A @ x == b]
    prob = cp.Problem(obj, cons)
    assert prob.is_dpp()
    layer = CvxpyLayer(prob, parameters=[Q_sqrt, q, A, b], variables=[x])
    return layer


def build_hardness_matrix(position, width, hard):
    dist = torch.cdist(position, position)+1e-8
    coulomb = 1 / dist
    width_square= width ** 2+1e-8
    factor = 1 / torch.sqrt(
        (
            width_square.reshape(width_square.shape[0], -1, 1)
            + width_square.reshape(width_square.shape[0], 1, -1)
        )
    )
    hard = hard + torch.diagonal(factor, dim1=-2, dim2=-1) * 2 / np.sqrt(np.pi).item()
    shield_factor = torch.special.erf(factor * dist)
    inter = coulomb * shield_factor
    inter = torch.diagonal_scatter(inter, hard, offset=0, dim1=-2, dim2=-1)
    return inter


class QPLayer(torch.nn.Module):
    """using single cvxpylaereys ,construct parameters"""

    def __init__(self, n_atom_max=5):
        super(QPLayer, self).__init__()
        self.opt = build_qp_layers(n_atom_max)

    def forward(self, inputs):
        """ inputs is a named tuple with filed
          postions B X P X 3
          width softness, negativity,B X P X 1
          total charge   B X 1
          """
        shape = inputs["_positions"].shape
        nbatch = shape[0]
        natom = shape[1]
        # get  parameters
        positions = inputs["_positions"]
        atom_width = inputs["atomic_width"].squeeze(-1)
        atomic_neg = inputs["atomic_neg"].squeeze(-1)
        atomic_hard = inputs["atomic_hard"].squeeze(-1)
        atom_mask = inputs["_atom_mask"]
        self.opt = build_qp_layers(natom)
        #build hardness matrix
        H = build_hardness_matrix(positions, atom_width, atomic_hard)
        mask=torch.einsum("ai,aj->aij",atom_mask,atom_mask)
        # mask padding atoms.
        Q = H*mask
        q = atomic_neg*atom_mask
        A = atom_mask.unsqueeze(dim=1)
        if "total_charge" not in inputs.keys():
            b = torch.zeros(nbatch, 1,device=A.device)
        else:
            b = inputs["total_charge"]
        charge = self.opt(Q, q, A, b)[0]
        result=charge.unsqueeze(-1) 
        return result
